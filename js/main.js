class GameObject {
    constructor(objectName, xPosition = 0, yPosition = 0) {
        this._name = objectName;
        this._xPos = xPosition;
        this._yPos = yPosition;
    }
    set xPos(xPosition) {
        this._xPos = xPosition;
    }
    set yPos(yPosition) {
        this._yPos = yPosition;
    }
    positionReset() {
        this._xPos = 0;
        this._yPos = 0;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        const image = document.createElement('img');
        image.src = `./images/${this._name}.png `;
        image.id = this._name + 'Img';
        this._element.appendChild(image);
        container.appendChild(this._element);
    }
    update() {
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
    }
}
class Car extends GameObject {
    constructor(name, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
    }
    move(xPosition) {
        this._xPos += xPosition;
    }
}
class Game {
    constructor() {
        this.keyDownHandler = (e) => {
            if (e.keyCode === 32) {
                this._carMoveRate += 50;
            }
        };
        this.keyUpHandler = (e) => {
            if (e.keyCode === 32) {
                if (this._currentTurn == 0) {
                    this._car.get("carOrange").move(this._carMoveRate);
                }
                if (this._currentTurn == 1) {
                    this._car.get("carWhite").move(this._carMoveRate);
                }
                this._carMoveRate = 0;
                this.update();
            }
            if (e.keyCode === 27) {
                this._stopGame = 1;
            }
        };
        this.loop = () => {
            this.collision();
            this.update();
            this.endGame();
            if (this._stopGame == 0) {
                requestAnimationFrame(this.loop);
            }
            else if (this._stopGame == 1) {
                console.log('Game has stopped');
            }
        };
        this.startGame = (e) => {
            if (e.button === 0 || e.button === 1 || e.button === 2) {
                this.removeGameMenu();
                window.addEventListener('keydown', this.keyDownHandler);
                window.addEventListener('keyup', this.keyUpHandler);
                this.draw();
                this.loop();
            }
        };
        this._car = new Map();
        this._car.set("carOrange", new Car("carOrange"));
        this._car.set("carWhite", new Car("carWhite"));
        this._parkingSpace = new ParkingSpace("parkingLot");
        this._scoreboard = new ScoreBoard("scoreBoard");
        this._wall = new Wall("wallLong");
        this._element = document.getElementById('container');
        this._carMoveRate = 0;
        this._currentTurn = 0;
        this._stopGame = 0;
        this._collision = false;
        this.gameMenu();
    }
    collision() {
        var carRect;
        const parkingRect = document.getElementById('parkingLot').getBoundingClientRect();
        const wallRect = document.getElementById('wallLong').getBoundingClientRect();
        if (this._currentTurn == 0) {
            carRect = document.getElementById('carOrange').getBoundingClientRect();
        }
        if (this._currentTurn == 1) {
            carRect = document.getElementById('carWhite').getBoundingClientRect();
        }
        if (carRect.right <= parkingRect.right && carRect.left >= parkingRect.left) {
            if (this._currentTurn == 0) {
                this._scoreboard.increaseScoreOf(this._currentTurn);
                this.changePlayer();
            }
            else if (this._currentTurn == 1) {
                this._scoreboard.increaseScoreOf(this._currentTurn);
                this.changePlayer();
            }
            this.update();
            console.log('Parked');
        }
        else {
            console.log('Not parked');
        }
        if (carRect.right >= wallRect.left) {
            if (this._currentTurn == 0) {
                this.changePlayer();
            }
            else if (this._currentTurn == 1) {
                this.changePlayer();
            }
            this.update();
            console.log('Wall collision');
        }
        else {
            console.log('No wall collision');
        }
    }
    draw() {
        this._parkingSpace.draw(this._element);
        this._scoreboard.draw(this._element);
        this.highlightPlayer();
        this._wall.draw(this._element);
        if (this._currentTurn == 0) {
            this._car.get("carOrange").draw(this._element);
        }
        if (this._currentTurn == 1) {
            this._car.get("carWhite").draw(this._element);
        }
    }
    update() {
        if (this._currentTurn == 0) {
            this._car.get("carOrange").update();
        }
        else if (this._currentTurn == 1) {
            this._car.get("carWhite").update();
        }
        this.collision();
        this._parkingSpace.update();
        this._scoreboard.update();
        this.highlightPlayer();
    }
    changePlayer() {
        if (this._currentTurn == 0) {
            this._car.get("carOrange").positionReset();
            var childImg = document.getElementById('carOrangeImg');
            var childDiv = document.getElementById('carOrange');
            childDiv.removeChild(childImg);
            this._element.removeChild(childDiv);
            this._car.get("carWhite").draw(this._element);
            this._currentTurn = 1;
        }
        else if (this._currentTurn == 1) {
            this._car.get("carWhite").positionReset();
            var childImg = document.getElementById('carWhiteImg');
            var childDiv = document.getElementById('carWhite');
            childDiv.removeChild(childImg);
            this._element.removeChild(childDiv);
            this._car.get("carOrange").draw(this._element);
            this._currentTurn = 0;
        }
    }
    unhighlightPlayers() {
        var element1 = document.getElementById("p1");
        var element2 = document.getElementById("p2");
        element1.removeAttribute("style");
        element2.removeAttribute("style");
    }
    highlightPlayer() {
        this.unhighlightPlayers();
        var element;
        if (this._currentTurn == 0) {
            element = document.getElementById("p1");
        }
        else if (this._currentTurn == 1) {
            element = document.getElementById("p2");
        }
        element.style.border = "2px dashed cyan";
        element.style.backgroundColor = "black";
    }
    endGame() {
        var score1 = this._scoreboard.getScore1();
        var score2 = this._scoreboard.getScore2();
        if (score1 == 5 || score2 == 5) {
            this._stopGame = 1;
        }
    }
    gameMenu() {
        var wrapper = document.createElement('div');
        wrapper.className = 'startMenu';
        wrapper.id = 'startMenu';
        var menuText = document.createElement('p');
        menuText.className = 'menuText';
        menuText.id = 'menuText';
        menuText.innerHTML = 'Welcome to a super hard 2-player car parking game. <br> Press START to play. <br> Game controls: <br> Hold \"space\" to accelerate. <br> Press \"ESC\" to quit.';
        var startButton = document.createElement('button');
        startButton.addEventListener('click', this.startGame);
        startButton.className = 'startButton';
        startButton.id = 'startButton';
        startButton.innerHTML = 'Start';
        wrapper.appendChild(menuText);
        wrapper.appendChild(startButton);
        this._element.appendChild(wrapper);
    }
    removeGameMenu() {
        var parentDiv = document.getElementById('startMenu');
        var child1 = document.getElementById('menuText');
        var child2 = document.getElementById('startButton');
        parentDiv.removeChild(child1);
        parentDiv.removeChild(child2);
        this._element.removeChild(parentDiv);
    }
}
const app = {};
(function () {
    let init = function () {
        app.game = new Game();
    };
    window.addEventListener('load', init);
})();
class ParkingSpace extends GameObject {
    constructor(name, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
    }
}
class ScoreBoard extends GameObject {
    constructor(name, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
        this._score1 = 0;
        this._score2 = 0;
    }
    getScore1() {
        return this._score1;
    }
    getScore2() {
        return this._score2;
    }
    increaseScoreOf(player) {
        if (player == 0) {
            this._score1 += 1;
        }
        else if (player == 1) {
            this._score2 += 1;
        }
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        const p0 = document.createElement('p');
        p0.innerHTML = 'The score\'s are: ';
        const p1 = document.createElement('p');
        p1.id = "p1";
        p1.innerHTML = 'Player1 = ';
        const p2 = document.createElement('p');
        p2.id = "p2";
        p2.innerHTML = 'Player2 = ';
        const span1 = document.createElement('span');
        span1.id = 'span1';
        span1.innerHTML = this._score1.toString();
        const span2 = document.createElement('span');
        span2.id = 'span2';
        span2.innerHTML = this._score2.toString();
        p1.appendChild(span1);
        p2.appendChild(span2);
        this._element.appendChild(p0);
        this._element.appendChild(p1);
        this._element.appendChild(p2);
        container.appendChild(this._element);
    }
    update() {
        var scoreSpan1 = document.getElementById('span1');
        var scoreSpan2 = document.getElementById('span2');
        scoreSpan1.innerHTML = this._score1.toString();
        scoreSpan2.innerHTML = this._score2.toString();
    }
}
class Wall extends GameObject {
    constructor(name, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
    }
}
//# sourceMappingURL=main.js.map